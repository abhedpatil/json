package Tests;

import Data.CustomerInformation;
import Util.JsonReader;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.simple.parser.ParseException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;

public class JsonTest {

    @Test(description = "simple json reader")
    public void JsonReader() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        CustomerInformation[] customerInformation = mapper.readValue(new File("D:\\Automation\\Projects\\json\\src\\test\\java\\Data\\CustomerInformation.json"), CustomerInformation[].class);

        //The values can now be passed in the test
        System.out.println(customerInformation[0].getfName());
        System.out.println(customerInformation[0].getlName());
        System.out.println(customerInformation[0].getEmail());
        System.out.println(customerInformation[0].getPhone());

        System.out.println(customerInformation[1].getfName());
        System.out.println(customerInformation[1].getlName());
        System.out.println(customerInformation[1].getEmail());
        System.out.println(customerInformation[1].getPhone());
    }


    @DataProvider(name = "registration data")
    public Object[][] passData() throws IOException, ParseException {
        //return JsonReader.getJSONdata(AppConfig.getJsonPath()+"Registration.json", "Registration Data",3);
        return JsonReader.getdata("D:\\Automation\\Projects\\json\\src\\test\\java\\Data\\Registration.json", "Registration Data", 2, 12);
    }

    @Test(dataProvider = "registration data")
    public void userRegistration(String FirstName, String LastName, String phone, String email, String address,
                                 String city, String state, String postalCode, String country, String userId,
                                 String pwd, String confirmPwd) {

        System.out.println(FirstName);
        System.out.println(LastName);
        System.out.println(phone);
        System.out.println(email);
        System.out.println(address);
        System.out.println(city);
        System.out.println(state);
        System.out.println(postalCode);
        System.out.println(country);
        System.out.println(userId);
        System.out.println(pwd);
        System.out.println(confirmPwd);
    }
}
